import React, { Fragment } from "react";
import ContentDetailJobPlacement from '../../../../Templates/LandingPage/ContentDetailJobPlacement';
import {
    Container,
} from 'reactstrap';
const JobPlacement = () => {
    return (
        <Fragment>
            <Container className="wrap-content">
                <ContentDetailJobPlacement />
            </Container>
        </Fragment>
    )
}
export default JobPlacement;