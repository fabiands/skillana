import React, { useState, useEffect, Fragment } from "react";
import LoadingAnimation from "../../../../../components/atoms/LoadingAnimation";
import request from "../../../../../utils/Request";
import requestGeneral from "../../../../../utils/RequestGeneral";
import UserGuide from "../../../../Templates/Spm/UserGuide";
import ResultSpm from "../../../../Templates/ResultAssessment/ResultAssessmentSpm";
import NoComplete from "../NoCompleteProfile";
function Spm() {
  const [loading, setLoading] = useState(true);
  const [isNotComplete, setNotComplete] = useState(false);
  const [hasDone, setHasDone] = useState(false);
  const [message, setMessage] = useState(null);
  const [result, setResult] = useState([]);
  useEffect(() => {
    requestGeneral
      .get(`/v2/users/validate-standart`)
      .then((res) => {
        request
          .get("/assessment/test/spm/validate")
          .then((res) => {
            if (res.data?.type) {
              const type = res.data.type_done;
              const exist = res.data.type;

              if (type === "HAS_DONE") {
                request
                  .get("/assessment/test/result_spm")
                  .then((res) => {
                    setResult(res.data.data);
                    setMessage(res.data.message);
                    setHasDone(true);
                    setLoading(false);
                  })
                  .catch((err) => {
                    setLoading(false);
                    setHasDone(false);
                  });
              } else if (type === "NOT_DONE") {
                if (exist === "DATA_EXIST") {
                  setLoading(false);
                  setHasDone(false);
                } else {
                  setLoading(false);
                  setHasDone(false);
                }
              }
            }
          })
          .catch((err) => {});
      })
      .catch((err) => {
        if (err.response?.status === 422) {
          setNotComplete(true);
          setLoading(false);
        } else {
          setLoading(false);
          return Promise.reject(err);
        }
      });
  }, []);

  return (
    <Fragment>
      {loading ? (
        <LoadingAnimation />
      ) : isNotComplete ? (
        <NoComplete />
      ) : hasDone ? (
        <ResultSpm result={result} back={true} message={message} />
      ) : (
        <UserGuide popup={true} />
      )}
    </Fragment>
  );
}

export default Spm;
