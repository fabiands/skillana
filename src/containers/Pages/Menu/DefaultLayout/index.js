import React, { Fragment } from "react";
import DefaultHeader from "./DefaultHeader";
import { AppHeader } from "@coreui/react";
import { Container } from "reactstrap";
import { Route } from "react-router";
import WhatsAppButton from "../../../Templates/LandingPage/WhatsAppButton";
const DefaultLayout = (props) => {
  const toUp = window.scrollTo(0, 0);
  return (
    <Fragment>
      {toUp}
      <div className="app" style={{ background: "#fff" }}>
        <AppHeader fixed>
          <DefaultHeader />
        </AppHeader>
        <div className="app-body container" style={{ paddingTop: 60 }}>
          <main style={{ width: "100%" }}>
            <Container fluid>
              <Route {...props} />
            </Container>
          </main>
        </div>
        {/* <ShortGuideUser /> */}
        <WhatsAppButton />
      </div>
    </Fragment>
  );
};
export default DefaultLayout;
