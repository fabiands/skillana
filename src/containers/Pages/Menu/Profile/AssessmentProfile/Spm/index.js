import React, { useState } from "react";
import { useEffect } from "react";
import { Alert } from "reactstrap";
import request from "../../../../../../utils/Request";
import LoadingAnimation from "../../../../../../components/atoms/LoadingAnimation";
import SpmResult from "../../../../../Templates/ResultAssessment/ResultAssessmentSpm";
import moment from "../../../../../../utils/Moment";
import { t } from "react-switch-lang";
function Spm(props) {
  const [loading, setLoading] = useState(true);
  const [result, setResult] = useState(null);
  moment.locale("id");
  useEffect(() => {
    request
      .get("/assessment/test/spm")
      .then((res) => {
        if (res.data?.data) {
          setResult(null);
          setLoading(false);
        }
      })
      .catch((err) => {
        if (err?.response?.status === 422) {
          request.get("/assessment/test/result_spm").then((res) => {
            const resultResponse = res.data.data;
            resultResponse["message"] = res.data.message;
            setResult(resultResponse);
            setLoading(false);
          });
        } else {
          setLoading(false);
          return Promise.reject(err);
        }
      });
  }, []);

  if (loading) {
    return <LoadingAnimation />;
  }

  return (
    <div className="pb-5 animated fadeIn">
      {result ? (
        <SpmResult result={result} back={false} message={result.message} />
      ) : (
        <Alert color="danger" className="text-center mt-3 mb-4">
          <h6 className="content-title mb-2">
            {t("Assessment.sorryNoAssessmentSpm")}
          </h6>
        </Alert>
      )}
    </div>
  );
}

export default Spm;
