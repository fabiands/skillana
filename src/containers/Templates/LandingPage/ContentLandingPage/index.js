import React, { Fragment } from 'react'
import {
    Row,
    Col,
} from 'reactstrap';
export default function NavbarLandingPage() {
    return (
        <Fragment>
            <Row className="content-web">
                <Col sm="12" className="text-center" style={{ paddingTop: 60, paddingBottom: 60 }}>
                    <img src={require("../../../../../assets/img/profesi.png")} style={{ width: '100%' }} alt="profesi" />
                </Col>
            </Row>
            <Row className="content-mobile">
                <Col sm="12" className="text-center" style={{ paddingTop: 60, paddingBottom: 60 }}>
                    <img src={require("../../../../../assets/img/pr-hp.png")} style={{ width: '100%' }} alt="profesi" />
                </Col>
            </Row>
        </Fragment>
    )
}