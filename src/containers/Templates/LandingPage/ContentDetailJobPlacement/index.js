import React, { useState, Fragment } from "react";
import { Row, Button, ListGroup } from "reactstrap";
import { Col } from "react-bootstrap";
import { translate, t } from "react-switch-lang";
import companyLogo from "../../../../assets/img/icons8-company.png";
import { connect } from "react-redux";
import { useEffect } from "react";
import { useParams } from "react-router-dom";
import requestGeneral from "../../../../utils/RequestGeneral";
import LoadingAnimation from "../../../../components/atoms/LoadingAnimation";
import ReactMarkdown from "react-markdown";

const JobDetail = (props) => {
  const [vacancy, setVacancy] = useState();
  const [loading, setLoading] = useState(true);
  const { id } = useParams();

  const getVacanciesId = async () => {
    try {
      const res = await requestGeneral.get(
        `/internal/recruitment/vacancies/${id}`
      );
      setVacancy(res.data.data);
      setLoading(false);
    } catch (err) {
      setVacancy(undefined);
      setLoading(false);
    }
  };

  useEffect(() => {
    getVacanciesId();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Fragment>
      {loading ? (
        <LoadingAnimation />
      ) : vacancy !== undefined ? (
        <Row className="job-detail-page">
          <Col lg="9">
            <Row className="job-detail-list">
              <Col lg="12">
                <ListGroup className="job-placement">
                  <Row>
                    <Col lg="2" sm="12">
                      {vacancy.company.logo ? (
                        <img
                          src={vacancy.company.logo}
                          alt="imageJob"
                          className="image-job-placement rounded"
                        />
                      ) : (
                        <img
                          src={companyLogo}
                          alt="imageJob"
                          className="image-job-placement rounded"
                        />
                      )}
                    </Col>
                    <Col lg="10" sm="12" style={{ marginTop: "1rem" }}>
                      <div className="job-placement-title">
                        <h1>{vacancy.name}</h1>
                        <h3>{vacancy.company.name}</h3>
                        <h2>
                          <i className="fa fa-map-marker"></i>{" "}
                          {`${
                            vacancy.province.name
                              ? vacancy.province.name
                              : "Tidak disebutkan"
                          }, ${
                            vacancy.city.name
                              ? vacancy.city.name
                              : " Tidak disebutkan"
                          }`}
                        </h2>
                      </div>
                      <div
                        className="job-placement-type"
                        style={{ marginTop: "2rem" }}
                      >
                        <Row>
                          <Col lg="4">
                            <p>{vacancy.company.type}</p>
                          </Col>
                          <Col lg="5">
                            {vacancy.showSalary ? (
                              <p>
                                IDR{" "}
                                {`${new Intl.NumberFormat(["ban", "id"]).format(
                                  vacancy.minSalary
                                )} - ${new Intl.NumberFormat([
                                  "ban",
                                  "id",
                                ]).format(vacancy.maxSalary)}`}
                              </p>
                            ) : (
                              <p>{t('JobVacancy.salaryNoMention')}</p>
                            )}
                          </Col>
                          <Col lg="3">
                            <p>{vacancy.type}</p>
                          </Col>
                        </Row>
                        <Row>
                          <Col lg="4">
                            <p>{`${t('JobVacancy.experience')} : ${vacancy.minYearExperience.toString()}`}</p>
                          </Col>
                          <Col lg="8">
                            <p>{`${t('JobVacancy.education')} : ${vacancy.minEducation.toUpperCase()} ${
                              vacancy.major
                            }`}</p>
                          </Col>
                        </Row>
                      </div>
                    </Col>
                  </Row>
                </ListGroup>
              </Col>
              <Col lg="12">
                <div className="job-description" style={{ marginTop: "2rem" }}>
                  <div className="job-description-title">
                    <h1>{t('JobVacancy.descQualification')}</h1>
                  </div>
                  <div
                    className="job-desc-list"
                    style={{ paddingLeft: "2.5rem" }}
                  >
                    <ReactMarkdown source={vacancy.description} />
                  </div>
                </div>
              </Col>
              <Col lg="12">
                <div className="job-description" style={{ marginTop: "2rem" }}>
                  <div className="job-description-title">
                    <h1>{t('JobVacancy.skillsRequired')}</h1>
                  </div>
                  <div className="job-desc-list">
                    <ul>
                      {vacancy.skills
                        ? vacancy.skills &&
                          vacancy.skills.map((skill) => (
                            <li className="tags-button" key={skill}>
                              {skill}
                            </li>
                          ))
                        : ""}
                    </ul>
                  </div>
                </div>
              </Col>
            </Row>
          </Col>
          <Col lg="3" className="button-side-group">
            <div className="button-group">
              <Button className="button-detail-daftar">APPLY NOW</Button>
            </div>
          </Col>
        </Row>
      ) : (
        <p>{t('JobVacancy.vacanciesNotAvailable')}</p>
      )}
    </Fragment>
  );
};

export default connect(({ user }) => ({ user }))(translate(JobDetail));
