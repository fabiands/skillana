import React, { useState, useEffect, useRef } from "react";
import { Container, Row, Col, Spinner } from "reactstrap";
import { translate, t } from "react-switch-lang";
import request from "../../../../../utils/Request";
import AffirmationModal from "../../../../../components/atoms/AffirmationModal";
import { useResultTestContext } from "../../Menu/AgilityTestContext";
import { toast } from "react-toastify";
import moment from "../../../../../utils/Moment";
import Pulse from "react-reveal/Pulse";
import { isMobile } from "react-device-detect";

toast.configure();
function Sankarta(props) {
  // eslint-disable-next-line
  const [result, setResult] = useResultTestContext({});
  const [errorInput, setErrorInput] = useState(false);
  // const [showButton, setShowButton] = useState(true);
  const vidRef = useRef(null);
  const [answers, setAnswers] = useState({});
  const [time, setTime] = useState({});
  const [startTime, setStartTime] = useState();
  const [error, setError] = useState(false);
  const [btnDisable, setBtnDisable] = useState(true);
  const [loading, setLoading] = useState(true);
  const [questions, setQuestions] = useState(null);
  const [section, setSection] = useState("topic");
  const [page, setPage] = useState(0);
  const [pageTitle, setPageTitle] = useState(1);
  const [completed, setCompleted] = useState(false);
  const [showBtnAnswer, setShowBtnAnswer] = useState(false);

  useEffect(() => {
    request
      .get("/assessment/test/agility/sankarta")
      .then((res) => {
        if (res.data?.data) {
          setLoading(false);
          setQuestions(res.data.data);
        }
      })
      .catch((err) => {
        setError(true);
      }); // eslint-disable-next-line
  }, []);

  function isError() {
    if (error) {
      return (
        <AffirmationModal
          isOpen={error}
          title={"Error !"}
          desc={t("Agility.errorConnection")}
          titleButton={t("General.okey")}
          toggleOpen={() => {
            setError(true);
          }}
          confirmation={() => {
            window.location.reload();
            setError(false);
          }}
        />
      );
    }
  }

  function isErrorInput() {
    if (errorInput) {
      return (
        <AffirmationModal
          isOpen={errorInput}
          title={"Error !"}
          desc={t("Agility.hasDataSankerta")}
          titleButton={t("General.okey")}
          toggleOpen={() => {
            setErrorInput(true);
          }}
          confirmation={() => {
            setErrorInput(false);
          }}
        />
      );
    }
  }

  function nextPage() {
    setLoading(true);
    window.scroll({ top: 0, behavior: "smooth" });
    if (section === "topic") {
      setStartTime(moment().format("YYYY-MM-DD HH:mm:ss"));
      setSection("asking");
      setBtnDisable(true);
      setLoading(false);
      // setShowButton(true);
      setShowBtnAnswer(false);
    } else {
      if (!answers[pageTitle]) {
        setLoading(false);
        setErrorInput(true);
        return false;
      } else {
        setTime((state) => ({
          ...state,
          [pageTitle]: moment(new Date(), "YYYY-MM-DD HH:mm:ss").diff(
            startTime,
            "seconds"
          ),
        }));
        if (questions[page].lastQuestion === true) {
          setCompleted(true);
          window.scroll({ top: 0, behavior: "smooth" });
          setLoading(false);
        } else {
          setPage(page + 1);
          setPageTitle(pageTitle + 1);
          setSection("topic");
          setBtnDisable(true);
          // setShowButton(true);
          setLoading(false);
        }
      }
    }
  }

  function chooseOption(option) {
    setAnswers((state) => ({
      ...state,
      [pageTitle]: option,
    }));
    setBtnDisable(false);
  }

  function isComplete() {
    if (completed) {
      return (
        <AffirmationModal
          isOpen={completed}
          title={"Success !"}
          desc={t("Agility.completeSankarta")}
          titleButton={t("General.okey")}
          toggleOpen={() => {
            setCompleted(true);
          }}
          confirmation={() => {
            let totalTime = 0;
            Object.values(time).map((data) => {
              totalTime = totalTime + parseInt(data);
              return true;
            });
            setResult((result) => ({
              ...result,
              sankarta: { answers, time, totalTime, step: props.page },
            }));
            window.scroll({ top: 0, behavior: "smooth" });
            props.setNextPage(false);
            toast.success("Success", {
              autoClose: 3000,
            });
          }}
        />
      );
    }
  }

  console.log(answers);
  return (
    <>
      {isMobile ? (
        <Container className="container-sankarta">
          {isError()}
          {isErrorInput()}
          {isComplete()}
          <Row>
            <Col xs="12" className=" mt-4 mb-4">
              <div
                className="d-flex flex-rows justify-content-center"
                style={{
                  width: "100%",
                  position: "relative",

                  zIndex: "10",
                }}
              >
                <h3 style={{ position: "fixed" }}>
                  <b>
                    {props.title} ( {pageTitle} / 2 )
                  </b>
                </h3>
              </div>
              <Row>
                {loading ? (
                  <Col sm="12" className="text-center">
                    <Spinner size="lg" />
                  </Col>
                ) : questions ? (
                  questions[page] ? (
                    <>
                      <Col sm="12" className="mt-4 mb-4 colVid">
                        <div className="vidwrapper">
                          <div
                            className="d-flex align-items-center"
                            style={{
                              position: "fixed",
                              width: "100vw",
                              height: "100vh",
                              left: 0,
                              top: 0,
                            }}
                          >
                            <div style={{ position: "relative" }}>
                              <video
                                ref={vidRef}
                                className="vid"
                                src={
                                  section === "topic"
                                    ? process.env.REACT_APP_DOMAIN +
                                      questions[page].videoTopic
                                    : process.env.REACT_APP_DOMAIN +
                                      questions[page].videoAsking
                                }
                                onContextMenu={(e) => e.preventDefault()}
                                style={{
                                  // position: "fixed",
                                  width: "100%",
                                  height: "100%",
                                  // left: 0,
                                  // top: 0,
                                }}
                                id="dataVid"
                                controls={false}
                                autoPlay={true}
                                muted
                                playsInline
                                onPause={
                                  section === "topic"
                                    ? () => nextPage()
                                    : () => setShowBtnAnswer(true)
                                }
                              />
                              <div
                                style={{
                                  position: "absolute",
                                  top: "50%",
                                  width: "100%",
                                }}
                              >
                                {section === "asking" ? (
                                  <Col
                                    xs="12"
                                    className={pageTitle === 2 ? "w-75" : ""}
                                    style={{
                                      // marginTop: "160px",
                                      zIndex: "30",
                                    }}
                                  >
                                    <Row className="px-3">
                                      {questions[page].options.map(
                                        (data, idx) => {
                                          return (
                                            <Col
                                              xs={pageTitle === 1 ? "6" : "12"}
                                              key={idx}
                                              className={
                                                pageTitle === 1
                                                  ? "text-center"
                                                  : "pl-0"
                                              }
                                            >
                                              {showBtnAnswer === true ? (
                                                <Pulse>
                                                  <button
                                                    id={`btnAnswer-${idx}`}
                                                    name={"option" + page}
                                                    type="button"
                                                    className={
                                                      pageTitle === 1
                                                        ? answers[pageTitle] ===
                                                          data.code
                                                          ? "options-sankarta options-sankarta-1 options-sankarta-1-selected"
                                                          : "options-sankarta options-sankarta-1"
                                                        : answers[pageTitle] ===
                                                          data.code
                                                        ? "options-sankarta options-sankarta-2 options-sankarta-2-selected"
                                                        : "options-sankarta options-sankarta-2"
                                                    }
                                                    style={
                                                      pageTitle === 1
                                                        ? {
                                                            fontSize: "10px",
                                                            width: "75%",
                                                          }
                                                        : {
                                                            fontSize: "7px",
                                                            width: "100%",
                                                          }
                                                    }
                                                    key={idx}
                                                    onClick={() => {
                                                      chooseOption(data.code);
                                                    }}
                                                  >
                                                    {data.text}
                                                  </button>
                                                </Pulse>
                                              ) : null}
                                            </Col>
                                          );
                                        }
                                      )}
                                    </Row>
                                  </Col>
                                ) : null}
                                {section === "asking" && showBtnAnswer ? (
                                  <Col sm="12" className="text-center">
                                    <button
                                      className="btn btn-primary btn-sm"
                                      onClick={nextPage}
                                      disabled={btnDisable}
                                    >
                                      {section === "asking"
                                        ? questions[page].lastQuestion
                                          ? "Submit"
                                          : t("General.next")
                                        : t("General.next")}{" "}
                                      <i className="fa fa-arrow-right ml-2"></i>
                                    </button>
                                  </Col>
                                ) : null}
                              </div>
                            </div>
                          </div>
                        </div>
                      </Col>
                    </>
                  ) : null
                ) : null}
              </Row>
            </Col>
          </Row>
        </Container>
      ) : (
        // Desktop Mode
        <Container>
          {isError()}
          {isErrorInput()}
          {isComplete()}
          <Row>
            <Col xs="12" className=" mt-4 mb-4">
              <div
                className="d-flex flex-rows justify-content-center"
                style={{
                  width: "100%",
                  position: "relative",

                  zIndex: "10",
                }}
              >
                <h3 style={{ position: "fixed" }}>
                  <b>
                    {props.title} ( {pageTitle} / 2 )
                  </b>
                </h3>
              </div>
              <Row className="p-5">
                {loading ? (
                  <Col sm="12" className="text-center">
                    <Spinner size="lg" />
                  </Col>
                ) : questions ? (
                  questions[page] ? (
                    <>
                      <Col sm="12" className="mt-4 mb-4 colVid">
                        <div className="vidwrapper">
                          <div
                            className="d-flex align-items-center justify-content-center"
                            style={{
                              position: "fixed",
                              width: "100vw",
                              height: "100vh",
                              left: 0,
                              top: 0,
                            }}
                          >
                            <div style={{ position: "relative" }}>
                              <video
                                ref={vidRef}
                                className="vid"
                                src={
                                  section === "topic"
                                    ? process.env.REACT_APP_DOMAIN +
                                      questions[page].videoTopic
                                    : process.env.REACT_APP_DOMAIN +
                                      questions[page].videoAsking
                                }
                                onContextMenu={(e) => e.preventDefault()}
                                style={{
                                  // position: "fixed",
                                  width: "100%",
                                  height: "100%",
                                  // left: 0,
                                  // top: 0,
                                }}
                                id="dataVid"
                                controls={false}
                                autoPlay={true}
                                muted
                                playsInline
                                onPause={
                                  section === "topic"
                                    ? () => nextPage()
                                    : () => setShowBtnAnswer(true)
                                }
                              />
                              <div
                                style={{
                                  position: "absolute",
                                  top: "50%",
                                  width: "100%",
                                }}
                              >
                                {section === "asking" ? (
                                  <Col
                                    sm="12"
                                    className={pageTitle === 2 ? "w-75" : ""}
                                    style={
                                      pageTitle === 1
                                        ? { marginTop: "50px", zIndex: 30 }
                                        : { marginTop: "20px", zIndex: 30 }
                                    }
                                  >
                                    <Row className="px-3">
                                      {questions[page].options.map(
                                        (data, idx) => {
                                          return (
                                            <Col
                                              xs={pageTitle === 1 ? "6" : "12"}
                                              className={
                                                pageTitle === 1
                                                  ? "text-center"
                                                  : "pl-0"
                                              }
                                            >
                                              {showBtnAnswer === true ? (
                                                <Pulse>
                                                  <button
                                                    id={`btnAnswer-${idx}`}
                                                    name={"option" + page}
                                                    type="button"
                                                    className={
                                                      pageTitle === 1
                                                        ? answers[pageTitle] ===
                                                          data.code
                                                          ? "options-sankarta options-sankarta-1 options-sankarta-1-selected"
                                                          : "options-sankarta options-sankarta-1"
                                                        : answers[pageTitle] ===
                                                          data.code
                                                        ? "options-sankarta options-sankarta-2 options-sankarta-2-selected"
                                                        : "options-sankarta options-sankarta-2"
                                                    }
                                                    style={{ padding: "10px" }}
                                                    key={idx}
                                                    onClick={() => {
                                                      chooseOption(data.code);
                                                    }}
                                                  >
                                                    {data.text}
                                                  </button>
                                                </Pulse>
                                              ) : null}
                                            </Col>
                                          );
                                        }
                                      )}
                                    </Row>
                                  </Col>
                                ) : null}
                                {section === "asking" && showBtnAnswer ? (
                                  <Col
                                    sm="12"
                                    className={`text-center mt-3 ${
                                      pageTitle === 2 ? `w-75` : ``
                                    }`}
                                  >
                                    <button
                                      className="btn btn-primary btn-lg"
                                      onClick={nextPage}
                                      disabled={btnDisable}
                                    >
                                      {section === "asking"
                                        ? questions[page].lastQuestion
                                          ? "Submit"
                                          : t("General.next")
                                        : t("General.next")}{" "}
                                      <i className="fa fa-arrow-right ml-2"></i>
                                    </button>
                                  </Col>
                                ) : null}
                              </div>
                            </div>
                          </div>
                        </div>
                      </Col>
                    </>
                  ) : null
                ) : null}
              </Row>
            </Col>
          </Row>
        </Container>
      )}
    </>
  );
}

export default translate(Sankarta);
