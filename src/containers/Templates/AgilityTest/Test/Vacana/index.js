import React, { useState, useEffect, useRef } from "react";
import { Container, Row, Col, Spinner } from "reactstrap";
import { translate, t } from "react-switch-lang";
import request from "../../../../../utils/Request";
import AffirmationModal from "../../../../../components/atoms/AffirmationModal";
import { useResultTestContext } from "../../Menu/AgilityTestContext";
import { toast } from "react-toastify";
import moment from "../../../../../utils/Moment";
import Pulse from "react-reveal/Pulse";
import { isMobile } from "react-device-detect";

toast.configure();
function Vacana(props) {
  // eslint-disable-next-line
  const [result, setResult] = useResultTestContext({});
  const [errorInput, setErrorInput] = useState(false);
  const [time, setTime] = useState({});
  const [startTime, setStartTime] = useState(props.startTime);
  const vidRef = useRef(null);
  const [answers, setAnswers] = useState({});
  const [error, setError] = useState(false);
  const [btnDisable, setBtnDisbale] = useState(true);
  const [loading, setLoading] = useState(true);
  const [questions, setQuestions] = useState(null);
  const [section, setSection] = useState("topic");
  const [page, setPage] = useState(0);
  const [pageQuestion, setPageQuestion] = useState(0);
  const [completed, setCompleted] = useState(false);
  const [showBtnAnswer, setShowBtnAnswer] = useState(false);

  useEffect(() => {
    request
      .get("/assessment/test/agility/vacana")
      .then((res) => {
        if (res.data?.data) {
          setLoading(false);
          setQuestions(res.data.data);
        }
      })
      .catch((err) => {
        setError(true);
      }); // eslint-disable-next-line
  }, []);
  function isError() {
    if (error) {
      return (
        <AffirmationModal
          isOpen={error}
          title={"Error !"}
          desc={t("Agility.errorConnection")}
          titleButton={t("General.okey")}
          toggleOpen={() => {
            setError(true);
          }}
          confirmation={() => {
            window.location.reload();
            setError(false);
          }}
        />
      );
    }
  }
  function isErrorInput() {
    if (errorInput) {
      return (
        <AffirmationModal
          isOpen={errorInput}
          title={"Error !"}
          desc={t("Agility.hasDataVacana")}
          titleButton={t("General.okey")}
          toggleOpen={() => {
            setErrorInput(true);
          }}
          confirmation={() => {
            setErrorInput(false);
          }}
        />
      );
    }
  }

  function isComplete() {
    if (completed) {
      return (
        <AffirmationModal
          isOpen={completed}
          title={"Success !"}
          desc={t("Agility.completeVacana")}
          titleButton={t("General.okey")}
          toggleOpen={() => {
            setCompleted(true);
          }}
          confirmation={() => {
            let totalTime = 0;
            Object.values(time).map((data) => {
              totalTime = totalTime + parseInt(data);
              return true;
            });
            setResult((result) => ({
              ...result,
              vacana: { answers, time, totalTime, step: props.page },
            }));
            window.scroll({ top: 0, behavior: "smooth" });
            props.setNextPage(false);
            toast.success("Success", {
              autoClose: 3000,
            });
          }}
        />
      );
    }
  }
  // console.log(time);
  function chooseOption(option) {
    setAnswers((state) => ({
      ...state,
      [questions[page].section +
      "." +
      questions[page]?.questions[pageQuestion].codeQuestion]: option,
    }));
    setBtnDisbale(false);
  }

  useEffect(() => {
    // eslint-disable-next-line
    if (Object.values(time).length > 0) {
      if (
        questions[page]?.questions[pageQuestion].lastQuestion === true &&
        questions[page]?.lastSection === true
      ) {
        setStartTime(moment().format("YYYY-MM-DD HH:mm:ss"));
      } else if (
        questions[page]?.questions[pageQuestion].lastQuestion === true &&
        questions[page]?.lastSection === false
      ) {
      } else {
        setStartTime(moment().format("YYYY-MM-DD HH:mm:ss"));
      }
    }
    // eslint-disable-next-line
  }, [time]);

  // useEffect(() => {
  //   if (answers[page] !== null && section === "asking") {
  //     nextPage()
  //   }
  // }, [answers])

  function nextPage() {
    setLoading(true);
    window.scroll({ top: 0, behavior: "smooth" });
    if (section === "topic") {
      setStartTime(moment().format("YYYY-MM-DD HH:mm:ss"));
      setSection("asking");
      setBtnDisbale(true);
      setLoading(false);

      setShowBtnAnswer(false);
    } else {
      if (
        !answers[
          questions[page].section +
            "." +
            questions[page]?.questions[pageQuestion].codeQuestion
        ]
      ) {
        setLoading(false);
        setErrorInput(true);
        return false;
      } else {
        if (
          questions[page]?.questions[pageQuestion].lastQuestion === true &&
          questions[page]?.lastSection === true
        ) {
          setTime((state) => ({
            ...state,
            [questions[page].section +
            "." +
            questions[page]?.questions[pageQuestion].codeQuestion]: moment(
              new Date(),
              "YYYY-MM-DD HH:mm:ss"
            ).diff(startTime, "seconds"),
          }));
          setCompleted(true);
          setLoading(false);
        } else if (
          questions[page]?.questions[pageQuestion].lastQuestion === true &&
          questions[page]?.lastSection === false
        ) {
          setTime((state) => ({
            ...state,
            [questions[page].section +
            "." +
            questions[page]?.questions[pageQuestion].codeQuestion]: moment(
              new Date(),
              "YYYY-MM-DD HH:mm:ss"
            ).diff(startTime, "seconds"),
          }));

          setBtnDisbale(true);
          setSection("topic");
          setPage(page + 1);
          setShowBtnAnswer(false);
          setPageQuestion(0);
          setLoading(false);
        } else {
          setTime((state) => ({
            ...state,
            [questions[page].section +
            "." +
            questions[page]?.questions[pageQuestion].codeQuestion]: moment(
              new Date(),
              "YYYY-MM-DD HH:mm:ss"
            ).diff(startTime, "seconds"),
          }));

          setBtnDisbale(true);
          setPageQuestion(pageQuestion + 1);
          setShowBtnAnswer(false);
          setLoading(false);
        }
      }
    }
  }
  // console.log(answers);
  return (
    <>
      {isMobile ? (
        <Container>
          {isError()}
          {isErrorInput()}
          {isComplete()}
          <Row>
            <Col xs="12" className="mt-4 mb-4">
              <h3
                style={{ zIndex: "10", position: "relative" }}
                onClick={() => console.log(questions[page].section)}
              >
                <b
                  style={
                    questions
                      ? questions[page]
                        ? questions[page].section === 2
                          ? questions[page].questions[pageQuestion]
                              .codeQuestion > 1
                            ? { position: "fixed", left: "55%" }
                            : { position: "fixed", left: "38%" }
                          : { position: "fixed", left: "40%" }
                        : { position: "fixed", left: "40%" }
                      : { position: "fixed", left: "40%" }
                  }
                >
                  {props.title} ({" "}
                  {loading ? (
                    <Spinner size="lg" />
                  ) : questions ? (
                    questions[page].section
                  ) : null}{" "}
                  / 4 )
                </b>
              </h3>
              <Row
                className={
                  questions
                    ? questions[page]
                      ? questions[page].section === 2
                        ? questions[page].questions[pageQuestion]
                            .codeQuestion >= 1
                          ? "p-0"
                          : null
                        : questions[page].section === 4
                        ? questions[page].questions[pageQuestion]
                            .codeQuestion >= 1
                          ? "px-5 pt-5 pb-0"
                          : "p-5"
                        : "p-5"
                      : "p-5"
                    : "p-5"
                }
              >
                {loading ? (
                  <Col sm="12" className="text-center">
                    <Spinner size="lg" />
                  </Col>
                ) : questions ? (
                  questions[page] ? (
                    <>
                      <Col
                        sm="12"
                        className={
                          questions[page].section === 2
                            ? questions[page].questions[pageQuestion]
                                .codeQuestion >= 2
                              ? "mb-4 mt-4"
                              : "m-0"
                            : questions[page].section === 1
                            ? questions[page].questions[pageQuestion]
                                .codeQuestion > 2
                              ? "mt-4"
                              : "mb-4 mt-4"
                            : "mb-4 mt-4"
                        }
                        style={
                          questions[page].section === 1
                            ? questions[page].questions[pageQuestion]
                                .codeQuestion > 2
                              ? { height: "0" }
                              : { height: "0" }
                            : questions[page].section === 2
                            ? questions[page].questions[pageQuestion]
                                .codeQuestion === 5
                              ? { height: "120px" }
                              : questions[page].questions[pageQuestion]
                                  .codeQuestion >= 2
                              ? { height: "40px" }
                              : { marginTop: 0, marginBottom: 0 }
                            : questions[page].section === 3
                            ? questions[page].questions[pageQuestion]
                                .codeQuestion >= 1
                              ? { height: 0 }
                              : null
                            : questions[page].section === 4
                            ? { height: "50px" }
                            : null
                        }
                      >
                        <div className="vidwrapper">
                          <div
                            className="d-flex align-items-center"
                            style={{
                              position: "fixed",
                              width: "100vw",
                              height: "100vh",
                              left: 0,
                              top: 0,
                            }}
                          >
                            <div style={{ position: "relative" }}>
                              <video
                                ref={vidRef}
                                className="vid"
                                onContextMenu={(e) => e.preventDefault()}
                                src={
                                  section === "topic"
                                    ? process.env.REACT_APP_DOMAIN +
                                      questions[page].videoTopic
                                    : process.env.REACT_APP_DOMAIN +
                                      questions[page].questions[pageQuestion]
                                        .video
                                }
                                style={{
                                  // position: "fixed",
                                  width: "100%",
                                  height: "100%",
                                  // left: 0,
                                  // top: 0,
                                }}
                                id="dataVid"
                                controls={false}
                                autoPlay={true}
                                muted
                                playsInline
                                onPause={
                                  section === "topic"
                                    ? () => nextPage()
                                    : () => setShowBtnAnswer(true)
                                }
                              />

                              <div
                                style={
                                  questions[page].section === 2 &&
                                  questions[page].questions[pageQuestion]
                                    .codeQuestion === 1
                                    ? {
                                        position: "absolute",
                                        top: "12%",
                                        width: "33%",
                                        right: "3%",
                                      }
                                    : {
                                        position: "absolute",
                                        top: "40%",
                                        width: "100%",
                                      }
                                }
                              >
                                {section === "asking" ? (
                                  <>
                                    <Row
                                      className={
                                        questions[page].section === 2 &&
                                        questions[page].questions[pageQuestion]
                                          .codeQuestion === 1
                                          ? "m-0"
                                          : ""
                                      }
                                    >
                                      {questions[page].section === 1 ? (
                                        questions[page].questions[pageQuestion]
                                          .codeQuestion > 2 ? (
                                          <Col sm="5" />
                                        ) : null
                                      ) : questions[page].section === 2 ? (
                                        questions[page].questions[pageQuestion]
                                          .codeQuestion >= 2 ? (
                                          <Col sm="6" />
                                        ) : null
                                      ) : questions[page].section >= 3 ? (
                                        questions[page].questions[pageQuestion]
                                          .codeQuestion >= 1 ? (
                                          <Col sm="5" />
                                        ) : null
                                      ) : null}
                                      <Col
                                        sm={
                                          questions[page].section === 2
                                            ? questions[page].questions[
                                                pageQuestion
                                              ].codeQuestion > 1
                                              ? "6"
                                              : "12"
                                            : questions[page].section === 1
                                            ? questions[page].questions[
                                                pageQuestion
                                              ].codeQuestion > 2
                                              ? "7"
                                              : "12"
                                            : questions[page].section >= 3
                                            ? questions[page].questions[
                                                pageQuestion
                                              ].codeQuestion >= 1
                                              ? "7"
                                              : "12"
                                            : null
                                        }
                                        className={
                                          questions[page].section === 1
                                            ? questions[page].questions[
                                                pageQuestion
                                              ].codeQuestion === 2
                                              ? "d-flex flex-rows p-3 justify-content-center"
                                              : questions[page].questions[
                                                  pageQuestion
                                                ].codeQuestion === 1
                                              ? "d-flex flex-column p-3"
                                              : "pt-0 ml-auto mr-5 mb-3 w-50"
                                            : questions[page].section === 2
                                            ? questions[page].questions[
                                                pageQuestion
                                              ].codeQuestion === 5
                                              ? "pt-0 ml-auto mr-4 mt-4 mb-3 w-50"
                                              : questions[page].questions[
                                                  pageQuestion
                                                ].codeQuestion >= 2
                                              ? "pt-0 ml-auto mr-4 mt-2 mb-3 w-50"
                                              : "pt-0 mb-3 px-0"
                                            : questions[page].section === 3
                                            ? questions[page].questions[
                                                pageQuestion
                                              ].codeQuestion >= 1
                                              ? "pt-0 ml-auto mr-5 mb-3 w-50"
                                              : "pt-0 ml-auto mr-5 mb-3 w-50"
                                            : questions[page].section === 3
                                            ? questions[page].questions[
                                                pageQuestion
                                              ].codeQuestion >= 1
                                              ? "pt-0 ml-auto mr-5 mb-3 w-50"
                                              : "pt-0 ml-auto mr-5 mb-3 w-50"
                                            : questions[page].section === 4
                                            ? questions[page].questions[
                                                pageQuestion
                                              ].codeQuestion >= 1
                                              ? "pt-0 ml-auto mr-4 mt-5 mb-3 w-50"
                                              : "pt-0 ml-auto mr-4 mt-5 mb-3 w-50"
                                            : "d-flex flex-column p-3 mt-0"
                                        }
                                        style={
                                          questions[page].section === 2
                                            ? questions[page].questions[
                                                pageQuestion
                                              ].codeQuestion < 2
                                              ? {
                                                  width: "100%",
                                                  height: "90px",
                                                }
                                              : null
                                            : null
                                        }
                                      >
                                        <Row
                                          className={
                                            questions[page].section === 2 &&
                                            questions[page].questions[
                                              pageQuestion
                                            ].codeQuestion === 1
                                              ? "px-0 mx-0"
                                              : "px-1"
                                          }
                                        >
                                          {questions[page]?.questions[
                                            pageQuestion
                                          ].options.map((data, idx) => {
                                            return showBtnAnswer === true ? (
                                              <Col
                                                xs={
                                                  12 /
                                                  questions[page]?.questions[
                                                    pageQuestion
                                                  ].options.length
                                                }
                                                lg="12"
                                                key={idx}
                                                className={`text center ${
                                                  questions[page].section ===
                                                    2 &&
                                                  questions[page].questions[
                                                    pageQuestion
                                                  ].codeQuestion === 1
                                                    ? `px-1`
                                                    : ``
                                                }`}
                                              >
                                                <Pulse>
                                                  <button
                                                    className={
                                                      questions[page]
                                                        .section === 1
                                                        ? questions[page]
                                                            ?.questions[
                                                            pageQuestion
                                                          ].codeQuestion === 2
                                                          ? answers[
                                                              `${questions[page].section}.${questions[page].questions[pageQuestion].codeQuestion}`
                                                            ] === data.name
                                                            ? "options-vacana options-vacana-selected mr-4 "
                                                            : "options-vacana mr-4"
                                                          : questions[page]
                                                              ?.questions[
                                                              pageQuestion
                                                            ].codeQuestion === 1
                                                          ? answers[
                                                              `${questions[page].section}.${questions[page].questions[pageQuestion].codeQuestion}`
                                                            ] === data.name
                                                            ? "options-vacana options-vacana-selected mb-0"
                                                            : "options-vacana mb-0"
                                                          : questions[page]
                                                              ?.questions[
                                                              pageQuestion
                                                            ].codeQuestion > 2
                                                          ? answers[
                                                              `${questions[page].section}.${questions[page].questions[pageQuestion].codeQuestion}`
                                                            ] === data.name
                                                            ? "options-vacana-x options-vacana-selected mb-0"
                                                            : "options-vacana-x mb-0"
                                                          : null
                                                        : questions[page]
                                                            .section >= 2
                                                        ? questions[page]
                                                            ?.questions[
                                                            pageQuestion
                                                          ].codeQuestion >= 1
                                                          ? answers[
                                                              `${questions[page].section}.${questions[page].questions[pageQuestion].codeQuestion}`
                                                            ] === data.name
                                                            ? "options-vacana options-vacana-selected mb-0"
                                                            : "options-vacana mb-0"
                                                          : null
                                                        : null
                                                    }
                                                    name={"option" + page}
                                                    key={idx}
                                                    onClick={() => {
                                                      chooseOption(data.name);
                                                      console.log(data.name);
                                                    }}
                                                    style={
                                                      questions[page]
                                                        .section === 2
                                                        ? questions[page]
                                                            ?.questions[
                                                            pageQuestion
                                                          ].codeQuestion === 1
                                                          ? {
                                                              backgroundColor:
                                                                "#FBAB34",
                                                              color: "white",
                                                              width: "100%",
                                                              marginRight:
                                                                "5px",
                                                              fontSize: "8px",
                                                            }
                                                          : null
                                                        : questions[page]
                                                            .section === 1
                                                        ? questions[page]
                                                            ?.questions[
                                                            pageQuestion
                                                          ].codeQuestion > 2
                                                          ? null
                                                          : null
                                                        : questions[page]
                                                            .section === 3 ||
                                                          questions[page]
                                                            .section === 4
                                                        ? questions[page]
                                                            ?.questions[
                                                            pageQuestion
                                                          ].codeQuestion >= 1
                                                          ? null
                                                          : null
                                                        : null
                                                    }
                                                  >
                                                    {data.text}
                                                  </button>
                                                </Pulse>
                                              </Col>
                                            ) : null;
                                          })}
                                        </Row>
                                      </Col>
                                    </Row>
                                  </>
                                ) : null}
                                {section === "asking" && showBtnAnswer ? (
                                  <Col
                                    sm="12"
                                    className={
                                      questions[page].section === 2
                                        ? questions[page]?.questions[
                                            pageQuestion
                                          ].codeQuestion >= 2
                                          ? "d-flex flex-rows justify-content-end pr-md-3 text-center mt-0"
                                          : "d-flex flex-rows justify-content-end text-center mt-0"
                                        : questions[page].section >= 3
                                        ? questions[page]?.questions[
                                            pageQuestion
                                          ].codeQuestion >= 1
                                          ? "d-flex flex-rows justify-content-end text-center mt-0"
                                          : null
                                        : questions[page].section === 1
                                        ? questions[page]?.questions[
                                            pageQuestion
                                          ].codeQuestion > 2
                                          ? "d-flex flex-rows justify-content-end text-center mt-0"
                                          : "text-center mt-0"
                                        : "text-center mt-0"
                                    }
                                  >
                                    <button
                                      className="btn btn-primary btn-sm"
                                      onClick={nextPage}
                                      disabled={btnDisable}
                                    >
                                      {section === "asking"
                                        ? questions[page].lastQuestion
                                          ? "Submit"
                                          : t("General.next")
                                        : t("General.next")}{" "}
                                      <i className="fa fa-arrow-right ml-2"></i>
                                    </button>
                                  </Col>
                                ) : null}
                              </div>
                            </div>
                          </div>
                        </div>
                      </Col>
                    </>
                  ) : null
                ) : null}
              </Row>
            </Col>
          </Row>
        </Container>
      ) : (
        // Desktop Mode
        <Container>
          {isError()}
          {isErrorInput()}
          {isComplete()}
          <Row>
            <Col xs="12" className=" mt-4 mb-4">
              <h3
                style={{
                  zIndex: "10",
                  position: "relative",
                  textAlign: "right",
                }}
              >
                <b>
                  {props.title} ({" "}
                  {loading ? (
                    <Spinner size="lg" />
                  ) : questions ? (
                    questions[page].section
                  ) : null}{" "}
                  / 4 )
                </b>
              </h3>
              <Row className="p-5">
                {loading ? (
                  <Col sm="12" className="text-center">
                    <Spinner size="lg" />
                  </Col>
                ) : questions ? (
                  questions[page] ? (
                    <>
                      <Col
                        sm="12"
                        className={
                          questions[page].section === 2
                            ? questions[page].questions[pageQuestion]
                                .codeQuestion >= 2
                              ? "mb-4 mt-4"
                              : "m-0"
                            : "mb-4 mt-4"
                        }
                        style={
                          questions[page].section === 1
                            ? questions[page].questions[pageQuestion]
                                .codeQuestion > 2
                              ? { height: "80px" }
                              : { height: "125px" }
                            : questions[page].section === 2
                            ? questions[page].questions[pageQuestion]
                                .codeQuestion === 5
                              ? { height: "200px" }
                              : questions[page].questions[pageQuestion]
                                  .codeQuestion >= 2
                              ? { height: "120px" }
                              : { marginTop: 0, marginBottom: 0 }
                            : questions[page].section === 3
                            ? questions[page].questions[pageQuestion]
                                .codeQuestion >= 1
                              ? { height: "120px" }
                              : null
                            : questions[page].section === 4
                            ? { height: "200px" }
                            : null
                        }
                      >
                        <div className="vidwrapper">
                          <div
                            className="d-flex align-items-center"
                            style={{
                              position: "fixed",
                              width: "100vw",
                              height: "100vh",
                              left: 0,
                              top: 0,
                            }}
                          >
                            <div style={{ position: "relative" }}>
                              <video
                                ref={vidRef}
                                className="vid"
                                onContextMenu={(e) => e.preventDefault()}
                                src={
                                  section === "topic"
                                    ? process.env.REACT_APP_DOMAIN +
                                      questions[page].videoTopic
                                    : process.env.REACT_APP_DOMAIN +
                                      questions[page].questions[pageQuestion]
                                        .video
                                }
                                style={{
                                  // position:"fixed",
                                  width: "100%",
                                  height: "100%",
                                  // left:0,
                                  // top:0,
                                }}
                                id="dataVid"
                                controls={false}
                                autoPlay={true}
                                muted
                                playsInline
                                onPause={
                                  section === "topic"
                                    ? () => nextPage()
                                    : () => setShowBtnAnswer(true)
                                }
                              />
                              <div
                                style={
                                  questions[page].section === 2 &&
                                  questions[page].questions[pageQuestion]
                                    .codeQuestion === 1
                                    ? {
                                        position: "absolute",
                                        top: "10%",
                                        width: "35%",
                                        right: "3%",
                                      }
                                    : {
                                        position: "absolute",
                                        top: "40%",
                                        width: "100%",
                                      }
                                }
                              >
                                {section === "asking" ? (
                                  <>
                                    {questions[page].section === 1 ? (
                                      questions[page].questions[pageQuestion]
                                        .codeQuestion > 2 ? (
                                        <Col sm="5" />
                                      ) : null
                                    ) : questions[page].section === 2 ? (
                                      questions[page].questions[pageQuestion]
                                        .codeQuestion >= 2 ? (
                                        <Col sm="6" />
                                      ) : null
                                    ) : questions[page].section >= 3 ? (
                                      questions[page].questions[pageQuestion]
                                        .codeQuestion >= 1 ? (
                                        <Col sm="6" />
                                      ) : null
                                    ) : null}
                                    <Col
                                      sm={
                                        questions[page].section === 2
                                          ? questions[page].questions[
                                              pageQuestion
                                            ].codeQuestion > 1
                                            ? "6"
                                            : "12"
                                          : questions[page].section === 1
                                          ? questions[page].questions[
                                              pageQuestion
                                            ].codeQuestion > 2
                                            ? "7"
                                            : "12"
                                          : questions[page].section >= 3
                                          ? questions[page].questions[
                                              pageQuestion
                                            ].codeQuestion >= 1
                                            ? "6"
                                            : "12"
                                          : null
                                      }
                                      className={
                                        questions[page].section === 1
                                          ? questions[page].questions[
                                              pageQuestion
                                            ].codeQuestion === 2
                                            ? "d-flex flex-rows p-3 mt-5 justify-content-center"
                                            : questions[page].questions[
                                                pageQuestion
                                              ].codeQuestion === 1
                                            ? "p-3 mt-3 ml-auto mr-1"
                                            : "pt-3 mt-3 ml-auto mr-1"
                                          : questions[page].section === 2
                                          ? questions[page].questions[
                                              pageQuestion
                                            ].codeQuestion === 5
                                            ? "pt-3 mt-5 ml-auto mr-1"
                                            : questions[page].questions[
                                                pageQuestion
                                              ].codeQuestion >= 2
                                            ? "pt-3 mt-3 ml-auto mr-1"
                                            : "pt-3 mt-0 mb-5 ml-auto mr-1"
                                          : "p-3 px-5 mt-5 ml-auto mr-5 mb-5"
                                      }
                                      style={
                                        questions[page].section === 2
                                          ? questions[page].questions[
                                              pageQuestion
                                            ].codeQuestion < 2
                                            ? { height: "100px" }
                                            : null
                                          : null
                                      }
                                    >
                                      <Row>
                                        {questions[page]?.questions[
                                          pageQuestion
                                        ].options.map((data, idx) => {
                                          return showBtnAnswer === true ? (
                                            <Col
                                              xs={
                                                12 /
                                                questions[page]?.questions[
                                                  pageQuestion
                                                ].options.length
                                              }
                                              key={idx}
                                              className={
                                                questions[page].section !== 1
                                                  ? "d-flex align-items-center"
                                                  : ""
                                              }
                                            >
                                              <Pulse>
                                                <button
                                                  className={
                                                    questions[page].section ===
                                                    1
                                                      ? questions[page]
                                                          ?.questions[
                                                          pageQuestion
                                                        ].codeQuestion === 2
                                                        ? answers[
                                                            `${questions[page].section}.${questions[page].questions[pageQuestion].codeQuestion}`
                                                          ] === data.name
                                                          ? "options-vacana options-vacana-selected mr-4 w-100"
                                                          : "options-vacana mr-4 w-100"
                                                        : questions[page]
                                                            ?.questions[
                                                            pageQuestion
                                                          ].codeQuestion === 1
                                                        ? answers[
                                                            `${questions[page].section}.${questions[page].questions[pageQuestion].codeQuestion}`
                                                          ] === data.name
                                                          ? "options-vacana options-vacana-selected mb-3 w-100"
                                                          : "options-vacana mb-3 w-100"
                                                        : questions[page]
                                                            ?.questions[
                                                            pageQuestion
                                                          ].codeQuestion > 2
                                                        ? answers[
                                                            `${questions[page].section}.${questions[page].questions[pageQuestion].codeQuestion}`
                                                          ] === data.name
                                                          ? "options-vacana-x options-vacana-selected mb-3 w-100"
                                                          : "options-vacana-x mb-3 w-100"
                                                        : null
                                                      : questions[page]
                                                          .section >= 2
                                                      ? questions[page]
                                                          ?.questions[
                                                          pageQuestion
                                                        ].codeQuestion >= 1
                                                        ? answers[
                                                            `${questions[page].section}.${questions[page].questions[pageQuestion].codeQuestion}`
                                                          ] === data.name
                                                          ? "options-vacana options-vacana-selected mb-3 w-100"
                                                          : "options-vacana mb-3 w-100"
                                                        : null
                                                      : null
                                                  }
                                                  name={"option" + page}
                                                  // key={idx}
                                                  onClick={() =>
                                                    chooseOption(data.name)
                                                  }
                                                  style={
                                                    questions[page].section ===
                                                    2
                                                      ? questions[page]
                                                          ?.questions[
                                                          pageQuestion
                                                        ].codeQuestion === 1
                                                        ? {
                                                            backgroundColor:
                                                              "#FBAB34",
                                                            color: "white",
                                                            width: "140px",
                                                            marginRight: "5px",
                                                          }
                                                        : null
                                                      : null
                                                  }
                                                >
                                                  {data.text}
                                                </button>
                                              </Pulse>
                                            </Col>
                                          ) : null;
                                        })}
                                      </Row>
                                    </Col>
                                  </>
                                ) : null}
                                {section === "asking" && showBtnAnswer ? (
                                  <Col
                                    sm="12"
                                    className={
                                      questions[page].section === 2
                                        ? questions[page]?.questions[
                                            pageQuestion
                                          ].codeQuestion >= 2
                                          ? "d-flex flex-rows justify-content-end pr-md-3 text-center mt-3"
                                          : "d-flex flex-rows justify-content-end text-center mt-3"
                                        : questions[page].section >= 3
                                        ? questions[page]?.questions[
                                            pageQuestion
                                          ].codeQuestion >= 1
                                          ? "d-flex flex-rows justify-content-end text-center mt-3"
                                          : null
                                        : "text-center mt-3"
                                    }
                                    style={
                                      questions[page].section === 1
                                        ? questions[page]?.questions[
                                            pageQuestion
                                          ].codeQuestion >= 3
                                          ? { paddingLeft: "30px" }
                                          : null
                                        : null
                                    }
                                  >
                                    <button
                                      className="btn btn-primary btn-lg"
                                      onClick={nextPage}
                                      disabled={btnDisable}
                                    >
                                      {section === "asking"
                                        ? questions[page].lastQuestion
                                          ? "Submit"
                                          : t("General.next")
                                        : t("General.next")}{" "}
                                      <i className="fa fa-arrow-right ml-2"></i>
                                    </button>
                                  </Col>
                                ) : null}
                              </div>
                            </div>
                          </div>
                        </div>
                      </Col>
                    </>
                  ) : null
                ) : null}
              </Row>
            </Col>
          </Row>
        </Container>
      )}
    </>
  );
}

export default translate(Vacana);
