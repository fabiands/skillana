import React, { useEffect, useState } from "react";
import requestGeneral from "../../../utils/RequestGeneral";
import importantLogo from "../../../assets/img/important-logo.svg";
import { Link } from "react-router-dom";
import { Row, Col, Alert } from "reactstrap";
import LoadingAnimation from "../../../components/atoms/LoadingAnimation";
import { translate } from "react-switch-lang";
function ValidateAccount(props) {
  const { t } = props;
  const [validate, setValidate] = useState(null);
  const [loading, setLoading] = useState(true);
  const getValidate = async () => {
    try {
      const res = await requestGeneral.get(`/v3/users/validation-assessment`);
      setValidate(res.data.type);
    } catch (err) {
      if (err.response?.status === 422) {
        setValidate(err.response.data.type);
      }
    } finally {
      setLoading(false);
    }
  };
  useEffect(() => {
    getValidate();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const validationList = {
    PROFILE_NOT_COMPLETE: {
      desc: t("Dashboard.validateProfile"),
      link: "/profile",
    },
    MBTI_EXPIRE: {
      desc: t("Dashboard.aptitudeExpire"),
      link: "/assessment-mbti",
    },
    PAPIKOSTICK_EXPIRE: {
      desc: t("Dashboard.workStyleExpire"),
      link: "/assessment-papikostik",
    },
    DISC_EXPIRE: {
      desc: t("Dashboard.personalityExpire"),
      link: "/assessment-disc",
    },
    MSDT_EXPIRE: {
      desc: t("Dashboard.leadershipExpire"),
      link: "/assessment-msdt",
    },
    SPM_EXPIRE: {
      desc: t("Dashboard.personalityExpire"),
      link: "/assessment-disc",
    },
    BUSINESS_INSIGHT_EXPIRE: {
      desc: t("Dashboard.businessInsightExpire"),
      link: "/assessment-business-insight",
    },
    MBTI_NOT_COMPLETE: {
      desc: t("Dashboard.aptitudeNotComplete"),
      link: "/assessment-mbti",
    },
    PAPIKOSTICK_NOT_COMPLETE: {
      desc: t("Dashboard.workStyleNotComplete"),
      link: "/assessment-papikostik",
    },
    DISC_NOT_COMPLETE: {
      desc: t("Dashboard.personalityNotComplete"),
      link: "/assessment-disc",
    },
    MSDT_NOT_COMPLETE: {
      desc: t("Dashboard.leadershipNotComplete"),
      link: "/assessment-msdt",
    },
    SPM_NOT_COMPLETE: {
      desc: t("Dashboard.personalityNotComplete"),
      link: "/assessment-disc",
    },
    BUSINESS_INSIGHT_NOT_COMPLETE: {
      desc: t("Dashboard.businessInsightNotComplete"),
      link: "/assessment-business-insight",
    },
  };
  return (
    <div>
      {loading ? (
        <LoadingAnimation />
      ) : (
        <>
          {validate !== "COMPLETE" ? (
            <Alert className="validate-account pb-1">
              <Row>
                <Col
                  lg="1"
                  sm="1"
                  md="1"
                  style={{ display: "flex", justifyContent: "center" }}
                >
                  <img
                    className="importantLogo"
                    src={importantLogo}
                    alt="important"
                  />
                </Col>
                <Col lg="11" sm="11" md="11">
                  <p className="mt-2">
                    {validationList[validate]?.desc + " "}
                    <Link
                      to={validationList[validate]?.link}
                      style={{ color: "blue" }}
                    >
                      {t("Dashboard.startNow")}
                    </Link>
                  </p>
                </Col>
              </Row>
            </Alert>
          ) : null}
        </>
      )}
    </div>
  );
}
export default translate(ValidateAccount);
