import React, { useState, useEffect } from "react";
import { Row, Col, Container, Alert } from "reactstrap";
import { useParams } from "react-router-dom";
import request from "../../../utils/Request";
import LoadingAnimation from "../../../components/atoms/LoadingAnimation";

function ShareableSpm() {
  const [loading, setLoading] = useState(true);
  const [result, setResult] = useState(null);
  const { resultCodes } = useParams();
  useEffect(() => {
    request
      .get("/assessment/test/spm/share/" + resultCodes)
      .then((res) => {
        setResult(res.data.data);
        setLoading(false);
      })
      .catch((err) => {
        setResult(null);
        setLoading(false);
      });
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div>
      {loading ? (
        <LoadingAnimation />
      ) : result ? (
        <Container>
          <div
            className="justify-content-center"
            style={{ paddingTop: "2rem" }}
          >
            <Row>
              <Col sm="2" />
              <Col sm="8">
                <div className="text-center mt-2">
                  <div className="background-img-result-spm">
                    <div className="middle-box">
                      <h3>Hasil Tes IQ Kamu</h3>
                      <div className="box-blue mt-3 mb-3">{result.type}</div>
                      <h4>Kamu masuk ke dalam kategori</h4>
                      <div className=" box-blue-young mt-2 mb-3">
                        {result.description}
                      </div>
                    </div>
                  </div>
                </div>
                <br />
              </Col>
              <Col sm="2" />
            </Row>
          </div>
        </Container>
      ) : (
        <Alert color="danger" className="text-center mt-3 mb-4">
          <h5 className="content-title mb-2">Maaf, data tidak ada</h5>
        </Alert>
      )}
    </div>
  );
}
export default ShareableSpm;
