import React, { Fragment } from "react";
import Webcam from "react-webcam";
import Countdown from "react-countdown";
import { Spinner, UncontrolledTooltip, Alert } from "reactstrap";
import { BrowserView, MobileView } from "react-device-detect";

export default function VideoWebcam(props) {
  console.log(props);
  const countdowns = React.useMemo(
    () => <Countdown date={Date.now() + 15000} renderer={props.renderer} />,
    [props.countdown] // eslint-disable-line react-hooks/exhaustive-deps
  );
  return (
    <Fragment>
      {props.warning ? (
        <Alert
          color="danger"
          className="warning-video text-center"
          style={{ width: "100%" }}
        >
          <p className="m-0">
            <b>
              Perhatian! Sisa waktu kamu sesi ini tinggal{" "}
              {props.countdown ? countdowns : null} detik lagi
            </b>
          </p>
        </Alert>
      ) : props.countdown ? (
        countdowns
      ) : null}
      {props.dominanHand === "kanan" ? (
        <div className="hand-background-kanan"></div>
      ) : props.dominanHand === "kiri" ? (
        <div className="hand-background-kiri"></div>
      ) : null}
      <BrowserView>
        <Webcam
          audio={true}
          ref={props.webcamRef}
          style={{
            height: "100vh",
            width: "100vw",
            position: "relative",
          }}
          videoConstraints={props.videoConstraints}
          mirrored={true}
        />
      </BrowserView>
      <MobileView>
        <Webcam
          audio={true}
          ref={props.webcamRef}
          style={{
            height: "100vh",
            width: "100vw",
            position: "relative",
          }}
          videoConstraints={props.videoConstraints}
          mirrored={true}
        />
      </MobileView>

      {props.visibleControlPlayer ? (
        <Fragment>
          <div className="box-control">
            <Fragment>
              {props.onStop ? (
                <Fragment>
                  <UncontrolledTooltip placement="top" target="stop">
                    Tombol Berhenti Rekam
                  </UncontrolledTooltip>
                  <Spinner type="grow" color="light" className="icon-spinner" />
                  <img
                    id="stop"
                    src={require("../../../../assets/img/stop.png")}
                    alt="stop"
                    onClick={props.handleStopCaptureClick}
                    style={{ width: 70 }}
                    className="icon-video-single"
                  />
                </Fragment>
              ) : props.onRecord ? (
                <>
                  <UncontrolledTooltip placement="top" target="start-video">
                    Tombol Mulai Rekam
                  </UncontrolledTooltip>
                  <Spinner type="grow" color="light" className="icon-spinner" />
                  <img
                    id="start-video"
                    src={require("../../../../assets/img/records.png")}
                    alt="start-video"
                    onClick={props.handleStartCaptureClick}
                    style={{ width: 70 }}
                    className="icon-video-single"
                  />
                </>
              ) : props.menuVideoPlayer ? (
                <div>
                  <Fragment>
                    <UncontrolledTooltip
                      placement="top"
                      target="icon-next-video"
                    >
                      Lanjut Palmistry
                    </UncontrolledTooltip>
                    <img
                      id="icon-next-video"
                      src={require("../../../../assets/img/icon-next.png")}
                      alt="icon-next-video"
                      style={{ width: 70 }}
                      onClick={props.handleNextVideo}
                      className="icon-next"
                    />
                  </Fragment>
                </div>
              ) : null}
            </Fragment>
          </div>
        </Fragment>
      ) : null}
    </Fragment>
  );
}
