import React, { useState, Fragment } from "react";
import { useEffect } from "react";
import Hint from "../../../components/atoms/Hint";
// import request from "../../../utils/Request";
import requestGeneral from "../../../utils/RequestGeneral";
import AffirmationModal from "../../../components/atoms/AffirmationModal";
import ChoiceModal from "../../../components/atoms/ChoiceModal";
import VideoWebcam from "./WebcamDummy";
import CaptureLoading from "../../../components/atoms/CaptureLoading";
import { isMobile, isBrowser } from "react-device-detect";
export default function DummyVideo() {
  const webcamRef = React.useRef(null);
  const mediaRecorderRef = React.useRef(null);
  const time = React.useRef(null);

  const [loading, setLoading] = useState(true);
  const [isDetect, setIsDetect] = useState(true);
  const [isNotSupport, setIsNotSupport] = useState(false);
  const [isNotConnect, setIsNotConnect] = useState(false);
  const [isPalmistry, setIsPalmistry] = useState(false);
  const [popUpPalmistry, setPopUpPalmistry] = useState(true);
  const [isFailedPalmistry, setIsFailedPalmistry] = useState(false);
  const [visibleControlPlayer, setVisibleControlPlayer] = useState(true);
  const [menuVideoPlayer, setMenuVideoPlayer] = useState(false);
  const [onRecord, setOnRecord] = useState(true);
  const [onStop, setOnStop] = useState(false);
  const [countdown, setCountdown] = useState(false);
  const [loopVideo, setLoopVideo] = useState(false);
  const [section, setSection] = useState(0);
  const [recordedChunks, setRecordedChunks] = useState([]);
  const [record, setRecord] = useState([]);
  const [popUpDominan, setPopUpDominan] = useState(false);
  const [dominanHand, setDominanHand] = useState(null);
  const [success, setSuccess] = useState(false);
  const [successVideo, setNotSuccessVideo] = useState(false);
  const [warning, setWarning] = useState(false);
  const [linkVideo, setLinkVideo] = useState(null);
  const [isNotSupportBrowser, setIsNotSupportBrowser] = useState(false);
  const videoConstraints = {
    facingMode: "user",
  };
  useEffect(() => {
    var isSafari =
      /constructor/i.test(window.HTMLElement) ||
      (function (p) {
        return p.toString() === "[object SafariRemoteNotification]";
      })(
        !window["safari"] ||
          (typeof safari !== "undefined" && window["safari"].pushNotification)
      );

    // Internet Explorer 6-11
    var isIE = /*@cc_on!@*/ false || !!document.documentMode;

    // Edge 20+
    var isEdge = !isIE && !!window.StyleMedia;
    if (isSafari || isIE || isEdge) {
      setIsNotSupportBrowser(true);
    }
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  //=================================================================================
  //=========================== function Loading Pop up ==============================
  //=================================================================================
  const showLoading = () => {
    if (loading) {
      return <CaptureLoading title="Loading" visible={true} />;
    }
  };

  //=================================================================================
  //=========================== function detect Device ==============================
  //=================================================================================
  // eslint-disable-next-line
  function detectDevice() {
    if (isDetect) {
      navigator.getUserMedia =
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia ||
        navigator.msGetUserMedia ||
        navigator.oGetUserMedia;
      if (navigator.getUserMedia) {
        // Request the camera.
        navigator.getUserMedia(
          // Constraints
          {
            video: true,
          },
          // Success Callback
          function (localMediaStream) {
            setIsDetect(false);
            setIsNotSupport(false);
            setLoading(false);
          },
          // Error Callback
          function (err) {
            setIsDetect(false);
            setIsNotSupport(true);
            setLoading(false);
          }
        );
      } else if (navigator.mediaDevices.getUserMedia) {
        // Request the camera.
        navigator.mediaDevices.getUserMedia(
          // Constraints
          {
            video: true,
          },
          // Success Callback
          function (localMediaStream) {
            setIsDetect(false);
            setIsNotSupport(false);
            setLoading(false);
          },
          // Error Callback
          function (err) {
            setIsDetect(false);
            setIsNotSupport(true);
            setLoading(false);
          }
        );
      } else {
        setIsDetect(false);
        setIsNotSupport(true);
        setLoading(false);
      }
    }
  }
  //=================================================================================
  //=========================== function not support ==============================
  //=================================================================================
  function showNotSupport() {
    if (isNotSupport) {
      return (
        <AffirmationModal
          isOpen={isNotSupport}
          title="Error !"
          desc="Maaf, periksa kembali pengaturan kamera pada perangkat kamu untuk meneruskan wawancara online ini. Setelah semua perangkat tersambung pada sistem kami, silakan refresh halaman website ini untuk melanjutkan wawancara online."
          titleButton="Kembali"
          toggleOpen={() => {
            setIsNotSupport(true);
          }}
          confirmation={() => {
            setIsNotSupport(false);
            window.location.replace("/home");
          }}
        />
      );
    }
  }
  function notSupportBrowser() {
    if (isNotSupportBrowser) {
      return (
        <AffirmationModal
          isOpen={isNotSupportBrowser}
          title="Error !"
          desc="Oops! Sepertinya browser yang kamu gunakan tidak mendukung fitur ini. Tapi jangan khawatir, kamu bisa menggunakan browser Chrome atau Firefox, ya
            "
          titleButton="Kembali"
          toggleOpen={() => {
            setIsNotSupportBrowser(true);
          }}
          confirmation={() => {
            setIsNotSupportBrowser(false);
            window.location.replace("/home");
          }}
        />
      );
    }
  }

  //=================================================================================
  //=========================== function not Connection ==============================
  //=================================================================================
  function showNotConnection() {
    if (isNotConnect) {
      return (
        <AffirmationModal
          isOpen={isNotConnect}
          title="Oops!"
          desc="Maaf, kamu tidak terhubung ke internet atau koneksi internet terganggu. Silahkan periksa koneksi internet kamu, ya!"
          titleButton="Kembali"
          toggleOpen={() => {
            setIsNotConnect(true);
          }}
          confirmation={() => {
            setIsNotConnect(false);
            window.location.replace("/home");
          }}
        />
      );
    }
  }

  //=================================================================================
  //========================= function pop up palmistry =============================
  //=================================================================================
  function popUpHintPalmistry() {
    if (popUpPalmistry) {
      return (
        <AffirmationModal
          isOpen={popUpPalmistry}
          title="Langkah terakhir nih"
          desc="Untuk mengakhiri wawancaramu arahkan telapak tanganmu ke kamera dan tekan tombol ambil gambar ya"
          titleButton="Oke"
          toggleOpen={() => {
            setPopUpPalmistry(true);
          }}
          confirmation={() => {
            setPopUpPalmistry(false);
            setPopUpDominan(true);
          }}
        />
      );
    }
  }
  //=================================================================================
  //========================= function pop up palmistry =============================
  //=================================================================================
  function popUpDominanHand() {
    if (popUpDominan) {
      return (
        <ChoiceModal
          isOpen={popUpDominan}
          title="Langkah terakhir nih"
          desc="Sebelum mengambil gambar telapak tangan kamu, beritahu kami tangan mana yang biasanya kamu gunakan dalam beraktifitas sehari-hari!"
          titleLeft="Tangan Kiri"
          titleRight="Tangan Kanan"
          onPressRight={() => {
            setPopUpDominan(false);
            setIsPalmistry(true);
            setDominanHand("kanan");
          }}
          onPressLeft={() => {
            setPopUpDominan(false);
            setIsPalmistry(true);
            setDominanHand("kiri");
          }}
        />
      );
    }
  }
  //=================================================================================
  //============================ pop up gagal palmistry =============================
  //=================================================================================
  function popUpFailedPalmistry() {
    if (isFailedPalmistry) {
      return (
        <AffirmationModal
          isOpen={isFailedPalmistry}
          title="Oops!"
          desc="Sepertinya kamu gagal mengambil gambar. Yuk, coba sekali lagi!"
          titleButton="Oke"
          toggleOpen={() => {
            setIsFailedPalmistry(true);
          }}
          confirmation={() => {
            setIsFailedPalmistry(false);
          }}
        />
      );
    }
  }

  const handleNextVideo = React.useCallback(() => {
    if (recordedChunks.length) {
      const blob = new Blob(recordedChunks, {
        type: "video/webm",
      });
      // const url = URL.createObjectURL(blob);
      let data = {
        video: blob,
      };
      setLoopVideo(false);
      setMenuVideoPlayer(false);
      setVisibleControlPlayer(false);
      setRecord([...record, data]);
      setRecordedChunks([]);
      setLoading(true);
    }
  }, [recordedChunks]); // eslint-disable-line react-hooks/exhaustive-deps
  //==================================================================================
  //=========================== function recorded video ==============================
  //==================================================================================
  const handleStartCaptureClick = React.useCallback(() => {
    setOnRecord(false);
    setCountdown(true);
    setMenuVideoPlayer(false);
    // setOnStop(true)
    var options = {
      mimeType: "video/webm",
    };
    mediaRecorderRef.current = new MediaRecorder(
      webcamRef.current.stream,
      options
    );
    mediaRecorderRef.current.addEventListener(
      "dataavailable",
      handleDataAvailable
    );
    mediaRecorderRef.current.start();
  }, [webcamRef, mediaRecorderRef]); // eslint-disable-line react-hooks/exhaustive-deps

  const handleDataAvailable = React.useCallback(
    ({ data }) => {
      if (data.size > 0) {
        setRecordedChunks((prev) => prev.concat(data));
      }
    },
    [setRecordedChunks]
  );

  //==================================================================================
  //============================= function stop video ================================
  //==================================================================================
  const handleStopCaptureClick = React.useCallback(() => {
    mediaRecorderRef.current.stop();
    setRecordedChunks([]);
    setWarning(false);
    clearInterval(time.current);
    setTimeout(() => {
      setMenuVideoPlayer(true);
    }, 500);
    setOnStop(false);
    setCountdown(false);
  }, [mediaRecorderRef, webcamRef]); // eslint-disable-line react-hooks/exhaustive-deps

  //==================================================================================
  //=========================== function Countdown video =============================
  //==================================================================================
  function renderer({ minutes, seconds, completed }) {
    if (completed) {
      // Render a completed state
      handleStopCaptureClick();
      setWarning(false);
      return "";
    } else {
      // Render a countdown
      return (
        <div className="box-control-up">
          <h2
            style={{ color: "#fff", margin: "0px" }}
            className="countdown-video"
          >
            {minutes}:{seconds}
          </h2>
        </div>
      );
    }
  }
  useEffect(() => {
    if (record.length > 0) {
      sendDataVideo();
    }
  }, [record]); // eslint-disable-line react-hooks/exhaustive-deps
  //=================================================================================
  //================================= send video ====================================
  //=================================================================================
  async function sendDataVideo() {
    let type;
    if (isMobile) {
      type = "mobile";
    } else if (isBrowser) {
      type = "web";
    }

    let formData = new FormData();
    formData.append(
      `video`,
      record[0]["video"],
      "video" + record[0]["code"] + ".webm"
    );
    formData.append(`type`, type);
    window.scrollTo(0, 0);
    setSection(section + 1);

    requestGeneral
      .post("v1/assessment_video/submit_palmistry_dummy", formData)
      .then((res) => {
        setLinkVideo(res.data.name_file);
        setSuccess(true);
        setLoading(false);
      })
      .catch((err) => {
        setNotSuccessVideo(true);
        setLoading(false);
      });
  }

  //=================================================================================
  //=================================== Success =====================================
  //=================================================================================
  function showOnSuccess() {
    if (success) {
      return (
        <Hint
          title="Selamat !"
          onClick={() => {
            setSuccess(false);
            window.location.replace("/home");
          }}
          backBtn={false}
          img="checked.png"
          visible={success}
          desc={linkVideo}
          actionTitle="Oke"
        />
      );
    }
  }

  //=================================================================================
  //============================== Pop up video error ===============================
  //=================================================================================
  function sendVideoError() {
    if (successVideo) {
      return (
        <AffirmationModal
          isOpen={successVideo}
          title="Oops !"
          desc="Maaf, sepertinya proses wawancara onlinemu sedikit terganggu, pastikan koneksi internetmu stabil ya !"
          titleButton="Oke"
          confirmation={() => {
            setNotSuccessVideo(false);
          }}
        />
      );
    }
  }
  return (
    <Fragment>
      <div className="player-wrapper">
        {notSupportBrowser()}
        {detectDevice()}
        {showNotSupport()}
        {showNotConnection()}
        {showLoading()}
        {showOnSuccess()}
        {sendVideoError()}
        {popUpHintPalmistry()}
        {popUpFailedPalmistry()}
        {popUpDominanHand()}
        {isPalmistry ? (
          <VideoWebcam
            webcamRef={webcamRef}
            videoConstraints={videoConstraints}
            loopVideo={loopVideo}
            visibleControlPlayer={visibleControlPlayer}
            onStop={onStop}
            onRecord={onRecord}
            warning={warning}
            setLoading={setLoading}
            handleStartCaptureClick={handleStartCaptureClick}
            renderer={renderer}
            countdown={countdown}
            handleStopCaptureClick={handleStopCaptureClick}
            menuVideoPlayer={menuVideoPlayer}
            handleNextVideo={handleNextVideo}
            dominanHand={dominanHand}
          />
        ) : null}
      </div>
    </Fragment>
  );
}
