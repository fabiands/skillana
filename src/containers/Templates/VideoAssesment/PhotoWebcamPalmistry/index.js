import React, { Fragment } from "react";
import Webcam from "react-webcam";
import { Spinner, UncontrolledTooltip } from "reactstrap";
import { t } from "react-switch-lang";
export default function PhotoWebcamPalmistry(props) {
  return (
    <Fragment>
      {props.dominanHand === "kanan" ? (
        <div className="hand-background-kanan"></div>
      ) : props.dominanHand === "kiri" ? (
        <div className="hand-background-kiri"></div>
      ) : null}
      <Webcam
        ref={props.webcamRefPhoto}
        screenshotFormat="image/jpeg"
        forceScreenshotSourceSize={true}
        mirrored={true}
        style={{ height: "100vh", width: "100vw", zIndex: 1 }}
        videoConstraints={props.videoConstraints}
      />
      <button
        className="btn btn-info btn-hands"
        onClick={props.setPopUpDominan}
      >
        <i className="fa fa-hand-paper-o mr-2"></i>
        {t("Interview.changeHand")}
      </button>
      <div className="box-control-palmistry">
        <Fragment>
          {props.capturing ? null : (
            <>
              <Spinner type="grow" color="light" className="icon-spinner" />
              <UncontrolledTooltip placement="top" target="photo">
                {t("Interview.buttonPhoto")}
              </UncontrolledTooltip>
              <img
                id="photo"
                src={require("../../../../assets/img/icon-camere.png")}
                alt="play"
                onClick={props.takePicture}
                style={{ width: 70 }}
                className="icon-video-single"
              />
            </>
          )}
        </Fragment>
      </div>
    </Fragment>
  );
}
