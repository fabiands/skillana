import React, { useEffect, useState } from "react";
import {
  Row,
  Col,
  Alert,
  Button,
  Container,
  DropdownItem,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
} from "reactstrap";
import request from "../../../../utils/Request";
import { Link } from "react-router-dom";
import {
  FacebookIcon,
  FacebookShareButton,
  TwitterShareButton,
  WhatsappShareButton,
  TwitterIcon,
  WhatsappIcon,
} from "react-share";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { toast } from "react-toastify";
import { translate, t } from "react-switch-lang";
export default translate(function ResultAssesmentSpm(props) {
  const [token, setToken] = useState(null);
  useEffect(() => {
    request
      .get("/token/" + props.result.id)
      .then((res) => {
        setToken(res.data);
      })
      .catch((err) => {});
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  // const [name, setName] = useState('Copy Link')
  const diKlik = () => {
    toast.success("Link Sudah Dicopy", { autoClose: 2000 });
  };

  const [dropdownOpen, setOpen] = useState(false);
  const toggle = () => setOpen(!dropdownOpen);

  return (
    <Container>
      <div className="justify-content-center" style={{ paddingTop: "2rem" }}>
        <Row>
          <Col sm="2">
            {props.back ? (
              <Link to="/assessment">
                <Button className="navtext btn btn-lg btn-netis-color mb-3">
                  <i className="fa fa-arrow-left mr-2"></i>Kembali
                </Button>
              </Link>
            ) : null}
          </Col>
          <Col sm="8">
            <Row>
              <Col sm="2" />
              <Col sm="8">
                <Alert
                  color="success"
                  className="text-center mb-3"
                  style={{
                    width: "100%",
                    marginRight: "auto",
                    marginLeft: "auto",
                  }}
                >
                  <h5 className="content-title mb-2">
                    {t('Assessment.congratulation')} <br /> {t('Assessment.iqTitle')}
                  </h5>
                  <p>
                    <b>{props.message}</b>
                  </p>
                </Alert>
                <Col sm="2" />
              </Col>
            </Row>
          </Col>
          <Col sm="2" className="mb-3">
            <ButtonDropdown
              isOpen={dropdownOpen}
              toggle={toggle}
              className="float-right"
            >
              <DropdownToggle
                caret
                className="navtext btn btn-lg btn-netis-color"
              >
                <i className="fa fa-share-alt mr-2"></i>Share
              </DropdownToggle>
              <DropdownMenu
                className="mr-4 mt-3"
                style={{ marginLeft: "-120px" }}
              >
                <DropdownItem style={{ border: "none" }}>
                  <FacebookShareButton
                    style={{ marginRight: 50 }}
                    className="sosmed text-left ml-1"
                    url={
                      process.env.REACT_APP_DOMAIN +
                      "/share-assessment-spm/" +
                      token
                    }
                    quote={"Inilah hasil tes IQ ku. Cek milikmu disini!"}
                  >
                    <FacebookIcon
                      size={32}
                      round={true}
                      style={{ marginRight: "1rem" }}
                    />
                    <b>Facebook</b>
                  </FacebookShareButton>{" "}
                  <br />
                </DropdownItem>
                <DropdownItem style={{ border: "none" }}>
                  <TwitterShareButton
                    style={{ marginRight: 50 }}
                    className="sosmed text-left ml-1"
                    url={
                      process.env.REACT_APP_DOMAIN +
                      "/share-assessment-spm/" +
                      token
                    }
                    title={"Inilah hasil tes IQ ku. Cek milikmu disini!"}
                  >
                    <TwitterIcon
                      size={32}
                      round={true}
                      style={{ marginRight: "1rem" }}
                    />
                    <b>Twitter</b>
                  </TwitterShareButton>
                  <br />
                </DropdownItem>
                <DropdownItem style={{ border: "none" }}>
                  <WhatsappShareButton
                    style={{ marginRight: 50 }}
                    className="sosmed text-left ml-1"
                    url={
                      process.env.REACT_APP_DOMAIN +
                      "/share-assessment-spm/" +
                      token
                    }
                    separator={"Inilah hasil tes IQ ku. Cek milikmu disini!"}
                  >
                    <WhatsappIcon
                      size={32}
                      round={true}
                      style={{ marginRight: "1rem" }}
                    />
                    <b>WhatsApp</b>
                  </WhatsappShareButton>
                </DropdownItem>
                <DropdownItem style={{ border: "none" }} onClick={diKlik}>
                  <CopyToClipboard
                    className="text-left ml-2"
                    text={
                      process.env.REACT_APP_DOMAIN +
                      "/share-assessment-spm/" +
                      token
                    }
                  >
                    <div>
                      <img
                        src={require("../../../../assets/img/copy.png")}
                        alt=""
                        style={{ width: "25px" }}
                      />
                      <b style={{ cursor: "pointer", marginLeft: "20px" }}>
                        Copy Link
                      </b>
                    </div>
                  </CopyToClipboard>
                </DropdownItem>
              </DropdownMenu>
            </ButtonDropdown>
          </Col>
        </Row>

        <Row>
          <Col sm="2" />
          <Col sm="8">
            <div className="text-center mt-2">
              <div className="background-img-result-spm">
                <div className="middle-box mt-3">
                  <h3 style={{ fontSize: "130%" }}>Hasil Tes IQ Kamu</h3>
                  <div className="box-blue mt-3 mb-3">{props.result.iq}</div>
                  <h4 style={{ fontSize: "130%" }}>
                    Kamu masuk ke dalam kategori
                  </h4>
                  <div className=" box-blue-young mt-2 mb-3">
                    {props.result.description}
                  </div>
                </div>
              </div>
            </div>
            <br />
          </Col>
          <Col sm="2" />
        </Row>
      </div>
    </Container>
  );
})
