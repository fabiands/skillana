import React, { useState } from "react";
import Spm from "../index";
import Hint from "../../../../components/atoms/Hint";
import {
  Button,
  Container,
  Row,
  Col,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import { translate } from "react-switch-lang";

function UserGuide(props) {
  const [nextPage, setNextPage] = useState(false);
  const [checkBox, setCheckBox] = useState(true);
  const [popup, setPopup] = useState(props.popup);
  if (nextPage) {
    return <Spm />;
  }
  const handleCheckBox = (e) => {
    if (e.target.checked === true) {
      setCheckBox(false);
    } else {
      setCheckBox(true);
    }
  };
  const { t } = props;
  function popupModal() {
    if (popup) {
      return (
        <Hint
          title="Satu langkah lagi!"
          onClick={() => {
            setPopup(false);
          }}
          isBack={true}
          onBack={() => history.back()}
          img={null}
          visible={popup}
          desc="Selanjutnya kamu bisa mengikut tes kemampuan kognitif atau Kembali tidak melanjutkan. Silahkan simak petunjuk pengerjaan soal dari kami, ya! Semangat!"
          actionTitle="Oke"
        />
      );
    }
  }
  return (
    <Container>
      {popupModal()}
      <Row>
        <Col sm="1"></Col>
        <Col
          sm="10"
          className=" mt-4 mb-4 pt-3 pb-3"
          style={{
            boxShadow: "0px 0px 4px 2px rgba(0, 0, 0, 0.1)",
            borderRadius: "12px",
          }}
        >
          <h2>
            <b>{t("Spm.letsGetStarted")}</b>
          </h2>
          <h6>{t("Spm.beforeStarting")}</h6>
          <br />
          <Row>
            <Col sm="8">
              <ul>
                <li>
                  <p>{t("Spm.beforeStartingPoint1")}</p>
                </li>
                <li>
                  <p>{t("Spm.beforeStartingPoint2")}</p>
                </li>
                <li>
                  <p>{t("Spm.beforeStartingPoint3")}</p>
                </li>
                <li>
                  <p>{t("Spm.beforeStartingPoint4")}</p>
                </li>
                <li>
                  <p>{t("Spm.beforeStartingPoint5")}</p>
                </li>
                <li>
                  <p>{t("Spm.beforeStartingPoint6")}</p>
                </li>
              </ul>
            </Col>
            <Col sm="4" className="img-guide-web">
              <img
                src={require("../../../../assets/img/guide-spm.png")}
                alt="guide"
                className="assetIQ text-center"
                // style={{ width: "100%" }}
              />
            </Col>
          </Row>
          <hr />
          <Row>
            <Col sm="8">
              <FormGroup check inline>
                <Label check>
                  <Input type="checkbox" onChange={(e) => handleCheckBox(e)} />{" "}
                  {t("Spm.beforeStartingAgreement")}
                </Label>
              </FormGroup>
            </Col>
            <Col sm="4" className="text-right">
              <Button
                className="btn btn-info btn-shadow pr-4 pl-4 "
                style={{ backgroundColor: "#5AADF7", color: "#fff" }}
                disabled={checkBox}
                onClick={() => {
                  window.scrollTo(0, 0);
                  setNextPage(true);
                }}
              >
                <b>
                  {t("Spm.btnContinue")}{" "}
                  <i className="fa fa-arrow-right ml-1"></i>
                </b>
              </Button>
            </Col>
          </Row>
        </Col>
        <Col sm="1"></Col>
      </Row>
    </Container>
  );
}

export default translate(UserGuide);
