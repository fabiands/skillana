import React, { useState, Fragment } from "react";
import "react-multi-carousel/lib/styles.css";
import ConfirmationModal from "../../../components/atoms/ConfirmationModal";
export default function ShortGuideUser() {
  const [isOpen, setIsOpen] = useState(false);
  const [hintNumber, setHintNumber] = useState(0);
  const open = () => {
    setIsOpen(!isOpen);
    setHintNumber(1);
  };
  function popUp() {
    if (isOpen) {
      if (hintNumber === 1) {
        return (
          <ConfirmationModal
            guide={1}
            isOpen={isOpen}
            title="Selamat Datang di Skillana"
            desc="Sebelum eksplor lebih jauh, apakah kamu mau menyimak panduan singkat penggunaan website Skillana ?"
            images={true}
            titleLeft="Lain Kali"
            titleRight="Lanjutkan"
            center={true}
            onPressRight={() => {
              setHintNumber(2);
            }}
            onPressLeft={() => setIsOpen(false)}
          />
        );
      }
      if (hintNumber === 2) {
        return (
          <ConfirmationModal
            guide={2}
            isOpen={isOpen}
            title="4 Langkah utama untuk memulai petualanganmu di Skillana."
            desc="Kamu sudah siap ? Ayo kita mulai!"
            images={true}
            titleLeft="Kembali"
            titleRight="Lanjutkan"
            center={true}
            onPressRight={() => {
              setHintNumber(3);
            }}
            onPressLeft={() => setHintNumber(1)}
          />
        );
      }
      if (hintNumber === 3) {
        return (
          <ConfirmationModal
            guide={3}
            isOpen={isOpen}
            title="Langkah pertama, Lengkapi profilmu"
            desc='Klik menu profilku dan pada tab "Profilku" kamu bisa melengkapi beberapa informasi dasar yang tersedia disana. Kamu juga bisa mengisi tab lain yang ada, namun usahakan tab profil sudah diisi semua ya!  '
            images={true}
            titleLeft="Kembali"
            titleRight="Lanjutkan"
            center={true}
            onPressRight={() => {
              setHintNumber(4);
            }}
            onPressLeft={() => setHintNumber(2)}
          />
        );
      }
      if (hintNumber === 4) {
        return (
          <ConfirmationModal
            guide={4}
            isOpen={isOpen}
            title="Langkah kedua, Lakukan Asesmen"
            desc="Setelah melengkapi profil, kamu bisa melakukan asesmen yang ada. Lakukan asesmen dengan tenang dan pahami dirimu lebih dalam."
            images={true}
            titleLeft="Kembali"
            titleRight="Lanjutkan"
            center={true}
            onPressRight={() => {
              setHintNumber(5);
            }}
            onPressLeft={() => setHintNumber(3)}
          />
        );
      }
      if (hintNumber === 5) {
        return (
          <ConfirmationModal
            guide={5}
            isOpen={isOpen}
            title="Langkah ketiga, Lakukan Pembuatan video CV"
            desc="Persiapkan dirimu dengan baik dan latih dirimu dengan fitur pembuatan video CV bersama Widya! Hasil video CV-mu, akan kami simpan dan bisa kamu gunakan untuk melamar pekerjaan."
            images={true}
            titleLeft="Kembali"
            titleRight="Lanjutkan"
            center={true}
            onPressRight={() => {
              setHintNumber(6);
            }}
            onPressLeft={() => setHintNumber(4)}
          />
        );
      }
      if (hintNumber === 6) {
        return (
          <ConfirmationModal
            guide={6}
            isOpen={isOpen}
            title="Langkah terakhir, Lamar pekerjaan yang kamu mau"
            desc="Tentukan lamaran pekerjaan mana yang ingin kamu lamar dari beberapa lamaran pekerjaan yang sedang tersedia dan tunggu. Semoga berhasil!"
            images={true}
            titleLeft="Kembali"
            titleRight="Oke"
            center={true}
            onPressRight={() => {
              setIsOpen(false);
            }}
            onPressLeft={() => setHintNumber(5)}
          />
        );
      }
    }
  }
  return (
    <Fragment>
      {popUp()}
      <div className="short-guide-container">
        <img
          className="ml-3"
          src={require("../../../assets/img/tanya.png")}
          alt="guide user"
          onClick={open}
        />
      </div>
    </Fragment>
  );
}
