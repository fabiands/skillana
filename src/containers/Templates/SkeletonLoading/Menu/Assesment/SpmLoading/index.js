import React from "react";
import Skeleton from "react-loading-skeleton";
import { Row, Col } from "reactstrap";
const SpmLoading = (props) => {
  return (
    <Row>
      <Col md={props.colrow} xs={6}>
        {Array(props.count)
          .fill()
          .map((item, index) => (
            <div className="col-sm-5ths text-center mb-4" key={index}>
              <Skeleton height={150} width={150} />
              <br />
              <div className="text-center">
                <Skeleton height={20} width={80} />
              </div>
              <Skeleton height={40} width={150} />
            </div>
          ))}
      </Col>
    </Row>
  );
};
export default SpmLoading;
