import React from "react";
import Skeleton from "react-loading-skeleton";
import { ListGroupItem, Row, Col } from "reactstrap";

const JobPlacement = () => {
  return (
    <section>
      {Array(5)
        .fill()
        .map((item, index) => (
          <ListGroupItem
            className="job-placement pt-4 pb-4 mb-2"
            key={index}
            style={{ borderRadius: "15px" }}
          >
            <Row>
              <Col sm="2 pb-3">
                <Skeleton height={`60%`} width={`100%`} />
              </Col>
              <Col sm="10">
                <Row>
                  <Col lg="9">
                    <Skeleton height={30} width={150} />
                    <br />
                    <Skeleton width={190} />
                    <br />
                    <Skeleton width={180} />
                    <br />
                    <br />
                    <Skeleton width={200} />
                  </Col>
                  <Col lg="3">
                    <Skeleton width={100} />
                    <br />
                    <Skeleton height={30} width={130} />
                  </Col>
                </Row>
              </Col>
            </Row>
          </ListGroupItem>
        ))}
    </section>
  );
};
export default JobPlacement;
