import React from "react";
import { Modal, ModalBody, Button, Row, Col, ModalFooter } from "reactstrap";
import { translate, t } from "react-switch-lang";
import { Link } from "react-router-dom";
import "./banner.css";
const BannerModal = (props) => {
  return (
    <Modal isOpen={props.isOpen} toggle={props.toggle} size="md">
      <ModalBody
        className={props.center ? "text-center p-1 img-bg" : "p-1 img-bg"}
      >
        <button
          type="button"
          class="close"
          aria-label="Close"
          style={{ position: "relative", zIndex: "999", marginLeft: "-30px" }}
          onClick={props.toggle}
        >
          <span aria-hidden="true">×</span>
        </button>
        <Link to={{ pathname: props.url }} target="_blank">
          <img
            src={require("../../../assets/img/banner/" + props.img)}
            width={props.width}
            height={props.height}
            alt="banner"
            className="cursor-pointer"
            style={{ borderRadius: "10px" }}
            onClick={props.toggle}
          />
        </Link>
      </ModalBody>
      <ModalFooter
        style={{ display: "inline-block", backgroundColor: "#13568fc2" }}
        className="pt-2 pb-2 pl-3 pr-3"
      >
        <Row>
          <Col xs="6" className="text-left">
            <Link
              style={{
                color: "#fff",
                position: "relative",
                top: "5px",
                textDecoration: "underline",
              }}
              className="mr-3"
              onClick={props.closeForToday}
            >
              {t("General.closeForToday")}
            </Link>
          </Col>
          <Col xs="6" className="text-right">
            <Button color="danger" onClick={props.toggle}>
              <b>
                <i className="fa fa-close mr-1"></i> {t("General.close")}
              </b>
            </Button>
          </Col>
        </Row>
      </ModalFooter>
    </Modal>
  );
};

export default translate(BannerModal);
