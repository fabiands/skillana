import React from "react";
import {
  Col
} from "reactstrap";
import { Link } from "react-router-dom";
import Color from "../../../utils/Colors";


//gadipake lagi
export default function CardMenu(props) {
  return (
    <Col md="4" >
        <Link to={props.link} >
            <div className={"card menu-item  " + props.css}>
                <div className="card-body">
                    <div className="menu-img mt-2 mb-3 d-flex justify-content-center" >
                        <img src={require("../../../assets/img/" + props.img + ".png")} alt=""/>
                    </div>
                    <div className="menu-title mb-2">
                        <h5 className="mb-2 title-menu-company" style={{ color: Color.primaryColor }}><b>{props.title}</b></h5>
                        <p style={{ fontWeight: 'normal' }}>
                            {props.desc}
                        </p>
                    </div>
                </div>
            </div>
        </Link>
    </Col>
  );
}
